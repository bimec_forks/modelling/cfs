PROJECT(Xerces-C C CXX)

cmake_minimum_required(VERSION 2.8)

include(CheckTypeSize)
include(CheckFunctionExists)
include(CheckIncludeFile)
include(CheckLibraryExists)
include(CheckCSourceCompiles)
include(CheckCXXSourceCompiles)

SET(PACKAGE "xerces-c")
SET(PACKAGE_BUGREPORT "")
SET(PACKAGE_NAME "xerces-c")
SET(PACKAGE_STRING "xerces-c 3.1.3")
SET(PACKAGE_TARNAME "xerces-c")
SET(PACKAGE_URL "")
SET(PACKAGE_VERSION "3.1.3")
SET(VERSION "3.1.3")

check_include_file(arpa/inet.h HAVE_ARPA_INET_H)
check_include_file(arpa/nameser_compat.h HAVE_ARPA_NAMESER_COMPAT_H)
check_include_file(CoreServices/CoreServices.h HAVE_CORESERVICES_CORESERVICES_H)
check_include_file(ctype.h HAVE_CTYPE_H)
check_include_file(dlfcn.h HAVE_DLFCN_H)
check_include_file(endian.h HAVE_ENDIAN_H)
check_include_file(errno.h HAVE_ERRNO_H)
check_include_file(fcntl.h HAVE_FCNTL_H)
check_include_file(float.h HAVE_FLOAT_H)
check_include_file(iconv.h HAVE_ICONV_H)
check_include_file(inttypes.h HAVE_INTTYPES_H)
check_include_file(langinfo.h HAVE_LANGINFO_H)
check_include_file(limits.h HAVE_LIMITS_H)
check_include_file(locale.h HAVE_LOCALE_H)
check_include_file(machine/endian.h HAVE_MACHINE_ENDIAN_H)
check_include_file(memory.h HAVE_MEMORY_H)
check_include_file(netdb.h HAVE_NETDB_H)
check_include_file(netinet/in.h HAVE_NETINET_IN_H)
check_include_file(nl_types.h HAVE_NL_TYPES_H)
check_include_file(stddef.h HAVE_STDDEF_H)
check_include_file(stdint.h HAVE_STDINT_H)
check_include_file(stdio.h HAVE_STDIO_H)
check_include_file(stdlib.h HAVE_STDLIB_H)
check_include_file(strings.h HAVE_STRINGS_H)
check_include_file(string.h HAVE_STRING_H)
check_include_file(sys/param.h HAVE_SYS_PARAM_H)
check_include_file(sys/socket.h HAVE_SYS_SOCKET_H)
check_include_file(sys/stat.h HAVE_SYS_STAT_H)
check_include_file(sys/timeb.h HAVE_SYS_TIMEB_H)
check_include_file(sys/time.h HAVE_SYS_TIME_H)
check_include_file(sys/types.h HAVE_SYS_TYPES_H)
check_include_file(unistd.h HAVE_UNISTD_H)
check_include_file(wchar.h HAVE_WCHAR_H)
check_include_file(wctype.h HAVE_WCTYPE_H)
check_include_file(winsock2.h HAVE_WINSOCK2_H)
check_include_file(emmintrin.h XERCES_HAVE_EMMINTRIN_H)
check_include_file(intrin.h XERCES_HAVE_INTRIN_H)
IF(NOT XERCES_HAVE_INTRIN_H)
  SET(XERCES_HAVE_INTRIN_H 0)
ENDIF()
check_include_file(cpuid.h HAVE_CPUID_H)
check_include_file(machine/endian.h HAVE_MACHINE_ENDIAN_H)
check_include_file(stdbool.h HAVE_STDBOOL_H)

IF(HAVE_INTTYPES_H)
  SET(XERCES_HAVE_INTTYPES_H 1)
ENDIF()

IF(HAVE_SYS_TYPES_H)
  SET(XERCES_HAVE_SYS_TYPES_H 1)
ENDIF()

check_function_exists(catclose HAVE_CATCLOSE)
check_function_exists(catgets HAVE_CATGETS)
check_function_exists(catopen HAVE_CATOPEN)
check_function_exists(clock_gettime HAVE_CLOCK_GETTIME)
check_function_exists(ftime HAVE_FTIME)
check_function_exists(getaddrinfo HAVE_GETADDRINFO)
check_function_exists(getcwd HAVE_GETCWD)
check_function_exists(gethostbyaddr HAVE_GETHOSTBYADDR)
check_function_exists(gethostbyname HAVE_GETHOSTBYNAME)
check_function_exists(gettimeofday HAVE_GETTIMEOFDAY)
check_function_exists(iconv HAVE_ICONV)
check_function_exists(iconv_close HAVE_ICONV_CLOSE)
check_function_exists(iconv_open HAVE_ICONV_OPEN)
check_function_exists(nsl HAVE_LIBNSL)
check_function_exists(socket HAVE_LIBSOCKET)
check_function_exists(localeconv HAVE_LOCALECONV)
check_function_exists(mblen HAVE_MBLEN)
check_function_exists(mbrlen HAVE_MBRLEN)
check_function_exists(mbsrtowcs HAVE_MBSRTOWCS)
check_function_exists(mbstowcs HAVE_MBSTOWCS)
check_function_exists(memmove HAVE_MEMMOVE)
check_function_exists(memset HAVE_MEMSET)
check_function_exists(nl_langinfo HAVE_NL_LANGINFO)
check_function_exists(pathconf HAVE_PATHCONF)
check_function_exists(realpath HAVE_REALPATH)
check_function_exists(setlocale HAVE_SETLOCALE)
check_function_exists(socket HAVE_SOCKET)
check_function_exists(strcasecmp HAVE_STRCASECMP)
check_function_exists(strchr HAVE_STRCHR)
check_function_exists(strdup HAVE_STRDUP)
check_function_exists(stricmp HAVE_STRICMP)
check_function_exists(strncasecmp HAVE_STRNCASECMP)
check_function_exists(strnicmp HAVE_STRNICMP)
check_function_exists(strrchr HAVE_STRRCHR)
check_function_exists(strstr HAVE_STRSTR)
check_function_exists(strtol HAVE_STRTOL)
check_function_exists(strtoul HAVE_STRTOUL)
check_function_exists(towlower HAVE_TOWLOWER)
check_function_exists(towupper HAVE_TOWUPPER)
check_function_exists(wcsicmp HAVE_WCSICMP)
check_function_exists(wcslwr HAVE_WCSLWR)
check_function_exists(wcsnicmp HAVE_WCSNICMP)
check_function_exists(wcsrtombs HAVE_WCSRTOMBS)
check_function_exists(wcstombs HAVE_WCSTOMBS)
check_function_exists(wcsupr HAVE_WCSUPR)

CHECK_TYPE_SIZE("int" SIZEOF_INT)
CHECK_TYPE_SIZE("long" SIZEOF_LONG)
CHECK_TYPE_SIZE("long long" SIZEOF_LONG_LONG)
CHECK_TYPE_SIZE("short" SIZEOF_SHORT)
CHECK_TYPE_SIZE("wchar_t" SIZEOF_WCHAR_T)
CHECK_TYPE_SIZE("__int64" SIZEOF___INT64)
IF(NOT SIZEOF___INT64)
  SET(SIZEOF___INT64 0)
ENDIF()
CHECK_TYPE_SIZE("size_t" SIZEOF_SIZE_T)
CHECK_TYPE_SIZE("ssize_t" SIZEOF_SSIZE_T)

SET(BOOL_SOURCE "
int f(int  x){return 1;}
int f(char x){return 1;}
int f(bool x){return 1;}

int
main ()
{
bool b = true; return f(b);
  ;
  return 0;
}
")

CHECK_CXX_SOURCE_COMPILES("${BOOL_SOURCE}" HAVE_BOOL) 

IF(NOT HAVE_BOOL)
  SET(XERCES_NO_NATIVE_BOOL 1)
ENDIF()


SET(LSTRING_SOURCE "
int
main ()
{
const wchar_t* s=L\"wide string\";
}
")
CHECK_CXX_SOURCE_COMPILES("${LSTRING_SOURCE}" HAVE_LSTRING) 

IF(HAVE_LSTRING)
  SET(XERCES_LSTRSUPPORT 1)
ENDIF()

SET(NAMESPACES_SOURCE "
namespace Outer { namespace Inner { int i = 0; }}
int
main ()
{
using namespace Outer::Inner; return i;
  ;
  return 0;
}
")
CHECK_CXX_SOURCE_COMPILES("${NAMESPACES_SOURCE}" HAVE_NAMESPACES) 

IF(HAVE_STDBOOL_H)
SET(STDBOOL_SOURCE "
#include <stdbool.h>
#ifndef bool
 \"error: bool is not defined\"
#endif
#ifndef false
 \"error: false is not defined\"
#endif
#if false
 \"error: false is not 0\"
#endif
#ifndef true
 \"error: true is not defined\"
#endif
#if true != 1
 \"error: true is not 1\"
#endif
#ifndef __bool_true_false_are_defined
 \"error: __bool_true_false_are_defined is not defined\"
#endif

        struct s { _Bool s: 1; _Bool t; } s;

        char a[true == 1 ? 1 : -1];
        char b[false == 0 ? 1 : -1];
        char c[__bool_true_false_are_defined == 1 ? 1 : -1];
        char d[(bool) 0.5 == true ? 1 : -1];
        bool e = &s;
        char f[(_Bool) 0.0 == false ? 1 : -1];
        char g[true];
        char h[sizeof (_Bool)];
        char i[sizeof s.t];
        enum { j = false, k = true, l = false * true, m = true * 256 };
        /* The following fails for
           HP aC++/ANSI C B3910B A.05.55 [Dec 04 2003]. */
        _Bool n[m];
        char o[sizeof n == m * sizeof n[0] ? 1 : -1];
        char p[-1 - (_Bool) 0 < 0 && -1 - (bool) 0 < 0 ? 1 : -1];
#       if defined __xlc__ || defined __GNUC__
         /* Catch a bug in IBM AIX xlc compiler version 6.0.0.0
            reported by James Lemley on 2005-10-05; see
            http://lists.gnu.org/archive/html/bug-coreutils/2005-10/msg00086.html
            This test is not quite right, since xlc is allowed to
            reject this program, as the initializer for xlcbug is
            not one of the forms that C requires support for.
            However, doing the test right would require a runtime
            test, and that would make cross-compilation harder.
            Let us hope that IBM fixes the xlc bug, and also adds
            support for this kind of constant expression.  In the
            meantime, this test will reject xlc, which is OK, since
            our stdbool.h substitute should suffice.  We also test
            this with GCC, where it should work, to detect more
            quickly whether someone messes up the test in the
            future.  */
         char digs[] = \"0123456789\";
         int xlcbug = 1 / (&(digs + 5)[-2 + (bool) 1] == &digs[4] ? 1 : -1);
#       endif
        /* Catch a bug in an HP-UX C compiler.  See
           http://gcc.gnu.org/ml/gcc-patches/2003-12/msg02303.html
           http://lists.gnu.org/archive/html/bug-coreutils/2005-11/msg00161.html
         */
        _Bool q = true;
        _Bool *pq = &q;

int
main ()
{

        *pq |= q;
        *pq |= ! q;
        /* Refer to every declared value, to avoid compiler optimizations.  */
        return (!a + !b + !c + !d + !e + !f + !g + !h + !i + !!j + !k + !!l
                + !m + !n + !o + !p + !q + !pq);

  ;
  return 0;
}"
)
CHECK_CXX_SOURCE_COMPILES("${STDBOOL_SOURCE}" HAVE__BOOL) 
ENDIF(HAVE_STDBOOL_H)

SET(STDNAMESPACE_SOURCE "
#include <iostream>
        std::istream& is = std::cin;

int
main ()
{
return 0;
  ;
  return 0;
}
")
CHECK_CXX_SOURCE_COMPILES("${STDNAMESPACE_SOURCE}" HAVE_STD_NAMESPACE) 

IF(HAVE_STD_NAMESPACE)
  SET(XERCES_STD_NAMESPACE 1)
ENDIF()


SET(STDLIBS_SOURCE "
#include <iostream>
#include <map>
#include <iomanip>
#include <cmath>")
IF(HAVE_NAMESPACES)
  SET(STDLIBS_SOURCE "${STDLIBS_SOURCE}
using namespace std;
")
ENDIF()
SET(STDLIBS_SOURCE "${STDLIBS_SOURCE}
int
main ()
{
return 0;
  ;
  return 0;
}
")
CHECK_CXX_SOURCE_COMPILES("${STDLIBS_SOURCE}" HAVE_STD_LIBS) 

IF(HAVE_STD_LIBS)
  SET(XERCES_NEW_IOSTREAMS 1)
ENDIF()


SET(ICONV_USES_CONST_POINTER_SOURCE "
#include <iconv.h>
int
main ()
{

                                       const char *fromPtr=0;
                                       size_t     fromLen=0;
                                       char       *toPtr=0;
                                       size_t     toLen=0;
                                       iconv_t    cv=0;
                                       iconv(cv, &fromPtr, &fromLen, &toPtr, &toLen);

  ;
  return 0;
}
")
CHECK_CXX_SOURCE_COMPILES("${ICONV_USES_CONST_POINTER_SOURCE}" ICONV_USES_CONST_POINTER) 
IF(ICONV_USES_CONST_POINTER)
  SET(ICONV_USES_CONST_POINTER 1)
ELSE()
  SET(ICONV_USES_CONST_POINTER 0)
ENDIF()

SET(STDC_HEADERS_SOURCE "
#include <ctype.h>
#include <stdlib.h>
#if ((' ' & 0x0FF) == 0x020)
# define ISLOWER(c) ('a' <= (c) && (c) <= 'z')
# define TOUPPER(c) (ISLOWER(c) ? 'A' + ((c) - 'a') : (c))
#else
# define ISLOWER(c) (('a' <= (c) && (c) <= 'i') || ('j' <= (c) && (c) <= 'r') || ('s' <= (c) && (c) <= 'z'))
# define TOUPPER(c) (ISLOWER(c) ? ((c) | 0x40) : (c))
#endif

#define XOR(e, f) (((e) && !(f)) || (!(e) && (f)))
int
main ()
{
  int i;
  for (i = 0; i < 256; i++)
    if (XOR (islower (i), ISLOWER (i))
        || toupper (i) != TOUPPER (i))
      return 2;
  return 0;
}")
CHECK_CXX_SOURCE_COMPILES("${STDC_HEADERS_SOURCE}" STDC_HEADERS) 

SET(TIMESYSTIME_SOURCE "
#include <sys/types.h>
#include <sys/time.h>
#include <time.h>

int
main ()
{
if ((struct tm *) 0)
return 0;
  ;
  return 0;
}")
CHECK_CXX_SOURCE_COMPILES("${TIMESYSTIME_SOURCE}" TIME_WITH_SYS_TIME) 

SET(XERCES_HAS_CPP_NAMESPACE 1)

IF(HAVE_CPUID_H AND NOT XERCES_HAVE_INTRIN_H)
  SET(XERCES_HAVE_GETCPUID_SOURCE "
#include <cpuid.h>
int
main ()
{
unsigned int eax, ebx, ecx, edx;
                                           __get_cpuid (1, &eax, &ebx, &ecx, &edx);

  ;
  return 0;
}")
  CHECK_CXX_SOURCE_COMPILES("${XERCES_HAVE_GETCPUID_SOURCE}" XERCES_HAVE_GETCPUID)
ENDIF(HAVE_CPUID_H AND NOT XERCES_HAVE_INTRIN_H)

SET(XERCES_HAVE_SSE2_INTRINSIC_SOURCE "
#include <emmintrin.h>
int
main ()
{
__m128i* one=(__m128i*)_mm_malloc(4, 16);
                                                                                   __m128i* two=(__m128i*)_mm_malloc(4, 16);
                                                                                   __m128i xmm1 = _mm_load_si128(one);
                                                                                   __m128i xmm2 = _mm_load_si128(two);
                                                                                   __m128i xmm3 = _mm_or_si128(xmm1, xmm2);
                                                                                   _mm_store_si128(one, xmm3);
                                                                                   _mm_free(one);
                                                                                   _mm_free(two);

  ;
  return 0;
}")
CHECK_CXX_SOURCE_COMPILES("${XERCES_HAVE_SSE2_INTRINSIC_SOURCE}" XERCES_HAVE_SSE2_INTRINSIC) 


IF(WIN32)
  IF(BUILD_SHARED_LIBS)
    SET(XERCES_PLATFORM_EXPORT "__declspec(dllexport)")
    SET(XERCES_PLATFORM_IMPORT "__declspec(dllimport)")
  ENDIF()

  SET(XERCES_USE_FILEMGR_WINDOWS 1)
  SET(XERCES_USE_MUTEXMGR_WINDOWS 1)
  SET(XERCES_USE_TRANSCODER_WINDOWS 1)
ELSE()
  SET(XERCES_USE_FILEMGR_POSIX 1)
  SET(XERCES_USE_MUTEXMGR_POSIX 1)
  IF(APPLE)
    SET(XERCES_USE_TRANSCODER_MACOSUNICODECONVERTER 1)
  ELSE()
    SET(XERCES_USE_TRANSCODER_GNUICONV 1)
  ENDIF()
ENDIF()


# IF(HAVE_ICONV_H AND HAVE_ICONV)
#   SET(XERCES_USE_MSGLOADER_ICONV 1)
# ENDIF()
SET(XERCES_USE_MSGLOADER_INMEMORY 1)

IF(HAVE_SOCKET)
  SET(XERCES_USE_NETACCESSOR_SOCKET 1)
ENDIF()

IF(HAVE_INTTYPES_H)
  SET(XERCES_S16BIT_INT "int16_t")
  SET(XERCES_S32BIT_INT "int32_t")
  SET(XERCES_S64BIT_INT "int64_t")
#  SET(XERCES_SIZE_T "size_t")
#  SET(XERCES_SSIZE_T "ssize_t")
  IF(NOT SIZEOF_SIZE_T)
    SET(XERCES_SIZE_T "long")
  ELSE()
    SET(XERCES_SIZE_T "size_t")
  ENDIF()

  IF(NOT SIZEOF_SSIZE_T)
    SET(XERCES_SSIZE_T "unsigned long")
  ELSE()
    SET(XERCES_SSIZE_T "ssize_t")
  ENDIF()
  SET(XERCES_U16BIT_INT "uint16_t")
  SET(XERCES_U32BIT_INT "uint32_t")
  SET(XERCES_U64BIT_INT "uint64_t")
ELSE()
  IF(SIZEOF_SHORT EQUAL 2)
    SET(XERCES_S16BIT_INT "short")
    SET(XERCES_U16BIT_INT "unsigned short")
  ELSEIF(SIZEOF_INT EQUAL 2)
    SET(XERCES_S16BIT_INT "int")
    SET(XERCES_U16BIT_INT "unsigned int")
  ELSE()
    SET(XERCES_S16BIT_INT "XERCES_INT32_TYPE_NOT_DEFINED")
    SET(XERCES_U16BIT_INT "XERCES_UINT32_TYPE_NOT_DEFINED")
  ENDIF()

  IF(SIZEOF_INT EQUAL 4)
    SET(XERCES_S32BIT_INT "int")
    SET(XERCES_U32BIT_INT "unsigned int")
  ELSEIF(SIZEOF_LONG EQUAL 4)
    SET(XERCES_S32BIT_INT "long")
    SET(XERCES_U32BIT_INT "unsigned long")
  ELSE()
    SET(XERCES_S32BIT_INT "XERCES_INT32_TYPE_NOT_DEFINED")
    SET(XERCES_U32BIT_INT "XERCES_UINT32_TYPE_NOT_DEFINED")
  ENDIF()

  IF(SIZEOF_LONG EQUAL 8)
    SET(XERCES_S64BIT_INT "long")
    SET(XERCES_U64BIT_INT "unsigned long")
  ELSEIF(SIZEOF_LONG_LONG EQUAL 8)
    SET(XERCES_S64BIT_INT "long long")
    SET(XERCES_U64BIT_INT "unsigned long long")
  ELSEIF(SIZEOF___INT64 EQUAL 8)
    SET(XERCES_S64BIT_INT "__int64")
    SET(XERCES_U64BIT_INT "unsigned __int64")
  ELSE()
    SET(XERCES_S64BIT_INT "XERCES_INT64_TYPE_NOT_DEFINED")
    SET(XERCES_U64BIT_INT "XERCES_UINT64_TYPE_NOT_DEFINED")
  ENDIF()

  IF(NOT SIZEOF_SIZE_T)
    SET(XERCES_SIZE_T "long")
  ELSE()
    SET(XERCES_SIZE_T "size_t")
  ENDIF()

  IF(NOT SIZEOF_SSIZE_T)
    SET(XERCES_SSIZE_T "unsigned long")
  ELSE()
    SET(XERCES_SSIZE_T "ssize_t")
  ENDIF()
ENDIF()


SET(XERCES_XMLCH_T_SOURCE "
#include <windows.h>
                                        wchar_t file[] = L\"dummy.file\";
int
main ()
{
DeleteFileW(file);
  ;
  return 0;
}")
CHECK_CXX_SOURCE_COMPILES("${XERCES_XMLCH_T_SOURCE}" XERCES_XMLCH_T) 
IF(XERCES_XMLCH_T)
  SET(XERCES_XMLCH_T "wchar_t")
  SET(XERCES_INCLUDE_WCHAR_H 1)
ELSE()
  SET(XERCES_XMLCH_T ${XERCES_U16BIT_INT})
ENDIF()

CHECK_LIBRARY_EXISTS(socket socket "" HAVE_LIBSOCKET) 
CHECK_LIBRARY_EXISTS(nsl gethostbyname "" HAVE_LIBNSL) 
CHECK_LIBRARY_EXISTS(pthread pthread_create "" HAVE_PTHREAD)


INCLUDE_DIRECTORIES(
  src
  ${CMAKE_CURRENT_BINARY_DIR}
  ${CMAKE_CURRENT_BINARY_DIR}/src
  )

CONFIGURE_FILE(
  ${CMAKE_CURRENT_SOURCE_DIR}/config_cmake.h.in
  ${CMAKE_CURRENT_BINARY_DIR}/config.h
  )
CONFIGURE_FILE(
  ${CMAKE_CURRENT_SOURCE_DIR}/src/xercesc/util/Xerces_autoconf_config_cmake.hpp.in
  ${CMAKE_CURRENT_BINARY_DIR}/src/xercesc/util/Xerces_autoconf_config.hpp
  )

IF(CMAKE_COMPILER_IS_GNUCXX)
  ADD_DEFINITIONS("-w")
ENDIF()

IF(MSVC)
  ADD_DEFINITIONS("-D_CRT_SECURE_NO_WARNINGS")
ENDIF()


ADD_SUBDIRECTORY(src)
